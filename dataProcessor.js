module.exports = {
    processData
};


function processData(itemList) {
    let value = 0;

    for (let i = 0; i < itemList.length; i++) {

        itemList[i].days -= 1;

        if (itemList[i].days <= 10) {

            if (itemList[i].days <= 1 || (itemList[i].available < 3 && itemList[i].days <= 5)) {
                itemList[i].price = itemList[i].full;
            } else {
                if (itemList[i].type == "CSD") {
                    itemList[i].price = itemList[i].full - (itemList[i].days * 30);
                } else {
                    itemList[i].price = itemList[i].full - (itemList[i].days * 20);
                }
            }

        } else if (itemList[i].days > 10) {

            if (itemList[i].days <= 1 || (itemList[i].available < 3 && itemList[i].days <= 5)) {
                itemList[i].price = itemList[i].full;
            } else {
                if (itemList[i].type == "CSM") {
                    itemList[i].price = itemList[i].full - 500;
                } else {
                    itemList[i].price = itemList[i].full - 400;
                }
            }
        }

        if (itemList[i].type == "CSD" && itemList[i].price < 900) {
            itemList[i].price = 900;
        } else if (itemList[i].type == "CSM" && itemList[i].price < 1000) {
            itemList[i].price = 1000;
        } else if (itemList[i].type = "CSPO" && itemList[i].price < 1200) {
            itemList[i].price = 1200;
        }

        value += (itemList[i].available * itemList[i].price);
    }
    return itemList;
}

