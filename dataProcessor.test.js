const dataProcessor = require('./dataProcessor.js');

test('freteGratis é verdadeiro para 200', () => {

  let i1 = {days: 15, seats: 30, available:10, online:true, type:"CSD", price:3000};
  let i2 = {days:9, seats:30, available:9, online:true, type:"CSPO", price:4000};
  let i3 = {days:8, seats:30, available:4, online:true, type:"CSM", price:3000};

  const items = [i1, i2, i3];

  expect(dataProcessor.processData(items)).toBeTruthy();
})
