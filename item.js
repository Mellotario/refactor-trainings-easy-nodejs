export class Item {
	constructor(days, seats, available, online, type, full) {
		this.days = days;
		this.seats = seats;
		this.available = available;
		this.online = online;
		this.type = type;
		this.price = full;
		this.full = full;
	}
}